from tensorflow.python.keras import layers
from tensorflow.python.keras import models
import loss_function as lf


def conv_block(input_tensor, num_filters):
    """
    Definition of the convolution block for the U-net architecture
    ConvBlock: two Conv2D in a row (3x3 filters, padding='same', ReLU)
    """

    encoder = layers.Conv2D(num_filters, (3, 3), padding='same', activation='relu', kernel_initializer='he_normal')(input_tensor)
    encoder = layers.Conv2D(num_filters, (3, 3), padding='same', activation='relu', kernel_initializer='he_normal')(encoder)
    return encoder


def encoder_block(input_tensor, num_filters):
    """
    Definition of the encoder block of the U-net 
    Encoder: Convolution Block --> Max Pooling (2x2 with stride of 2)
    """
    
    encoder = conv_block(input_tensor, num_filters)
    encoder_pool = layers.MaxPooling2D((2, 2), strides=(2, 2))(encoder)
    return encoder_pool, encoder


def decoder_block(input_tensor, concat_tensor, num_filters):  # Build decoder block
    """
    Definition of the decoder block of the U-net 
    Decoder: Transpose2DConvoltion --> Concatenation --> Convolution Block
    """
    
    decoder = layers.Conv2DTranspose(num_filters, (2, 2), strides=(2, 2), padding='same',
                                     activation='relu', kernel_initializer='he_normal')(input_tensor)
    decoder = layers.concatenate([concat_tensor, decoder], axis=-1)
    decoder = conv_block(decoder, num_filters)
    return decoder


def model_type(parameters):
    """
    Definition of the U-Net model types
    """
    
    # get parameters
    net_type = parameters['net_type']
    img_size = parameters['img_size']
    if parameters['new_class']:
        nb_classes = parameters['nb_classes'] + 1
    else:            
        nb_classes = parameters['nb_classes']

    # Define U-Net types (the numbers are different from shown in supplement S1)
    if net_type == 1:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 64)
        encoder1_pool, encoder1 = encoder_block(encoder0_pool, 128)
        encoder2_pool, encoder2 = encoder_block(encoder1_pool, 256)
        encoder3_pool, encoder3 = encoder_block(encoder2_pool, 512)
        center = conv_block(encoder3_pool, 1024)
        decoder3 = decoder_block(center, encoder3, 512)
        decoder2 = decoder_block(decoder3, encoder2, 256)
        decoder1 = decoder_block(decoder2, encoder1, 128)
        decoder0 = decoder_block(decoder1, encoder0, 64)
        outputs = layers.Conv2D(nb_classes,(1,1),activation='softmax')(decoder0)
        
    elif net_type == 2:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 32)
        encoder1_pool, encoder1 = encoder_block(encoder0_pool, 64)
        encoder2_pool, encoder2 = encoder_block(encoder1_pool, 128)
        encoder3_pool, encoder3 = encoder_block(encoder2_pool, 256)
        center = conv_block(encoder3_pool, 512)
        decoder3 = decoder_block(center, encoder3, 256)
        decoder2 = decoder_block(decoder3, encoder2, 128)
        decoder1 = decoder_block(decoder2, encoder1, 64)
        decoder0 = decoder_block(decoder1, encoder0, 32)
        outputs = layers.Conv2D(nb_classes,(1,1),activation='softmax')(decoder0)

    elif net_type == 3:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 32)
        encoder1_pool, encoder1 = encoder_block(encoder0_pool, 64)
        encoder2_pool, encoder2 = encoder_block(encoder1_pool, 128)
        encoder3_pool, encoder3 = encoder_block(encoder2_pool, 256)
        center = conv_block(encoder3_pool, 256)
        decoder3 = decoder_block(center, encoder3, 256)
        decoder2 = decoder_block(decoder3, encoder2, 128)
        decoder1 = decoder_block(decoder2, encoder1, 64)
        decoder0 = decoder_block(decoder1, encoder0, 32)
        outputs = layers.Conv2D(nb_classes,(1,1),activation='softmax')(decoder0)

    elif net_type == 4:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 16)
        encoder1_pool, encoder1 = encoder_block(encoder0_pool, 32)
        encoder2_pool, encoder2 = encoder_block(encoder1_pool, 64)
        encoder3_pool, encoder3 = encoder_block(encoder2_pool, 128)
        center = conv_block(encoder3_pool, 256)
        decoder3 = decoder_block(center, encoder3, 128)
        decoder2 = decoder_block(decoder3, encoder2, 64)
        decoder1 = decoder_block(decoder2, encoder1, 32)
        decoder0 = decoder_block(decoder1, encoder0, 16)
        outputs = layers.Conv2D(nb_classes,(1,1),activation='softmax')(decoder0)
        
    elif net_type == 5:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 128)
        encoder1_pool, encoder1 = encoder_block(encoder0_pool, 256)
        encoder2_pool, encoder2 = encoder_block(encoder1_pool, 512)
        center = conv_block(encoder2_pool, 1024)
        decoder2 = decoder_block(center, encoder2, 512)
        decoder1 = decoder_block(decoder2, encoder1, 256)
        decoder0 = decoder_block(decoder1, encoder0, 128)
        outputs = layers.Conv2D(nb_classes,(1,1),activation='softmax')(decoder0)

    elif net_type == 6:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 64)
        encoder1_pool, encoder1 = encoder_block(encoder0_pool, 128)
        encoder2_pool, encoder2 = encoder_block(encoder1_pool, 256)
        center = conv_block(encoder2_pool, 512)
        decoder2 = decoder_block(center, encoder2, 256)
        decoder1 = decoder_block(decoder2, encoder1, 128)
        decoder0 = decoder_block(decoder1, encoder0, 64)
        outputs = layers.Conv2D(nb_classes, (1, 1), activation='softmax')(decoder0)

    elif net_type == 7:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 32)
        encoder1_pool, encoder1 = encoder_block(encoder0_pool, 64)
        encoder2_pool, encoder2 = encoder_block(encoder1_pool, 128)
        center = conv_block(encoder2_pool, 256)
        decoder2 = decoder_block(center, encoder2, 128)
        decoder1 = decoder_block(decoder2, encoder1, 64)
        decoder0 = decoder_block(decoder1, encoder0, 32)
        outputs = layers.Conv2D(nb_classes,(1,1),activation='softmax')(decoder0)

    elif net_type == 8:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 16)
        encoder1_pool, encoder1 = encoder_block(encoder0_pool, 32)
        encoder2_pool, encoder2 = encoder_block(encoder1_pool, 64)
        center = conv_block(encoder2_pool, 128)
        decoder2 = decoder_block(center, encoder2, 64)
        decoder1 = decoder_block(decoder2, encoder1, 32)
        decoder0 = decoder_block(decoder1, encoder0, 16)
        outputs = layers.Conv2D(nb_classes,(1,1),activation='softmax')(decoder0)


    elif net_type == 9:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 128)
        encoder1_pool, encoder1 = encoder_block(encoder0_pool, 256)
        center = conv_block(encoder1_pool, 512)
        decoder1 = decoder_block(center, encoder1, 256)
        decoder0 = decoder_block(decoder1, encoder0, 128)
        outputs = layers.Conv2D(nb_classes, (1, 1), activation='softmax')(decoder0)

    elif net_type == 10:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 64)
        encoder1_pool, encoder1 = encoder_block(encoder0_pool, 128)
        center = conv_block(encoder1_pool, 256)
        decoder1 = decoder_block(center, encoder1, 128)
        decoder0 = decoder_block(decoder1, encoder0, 64)
        outputs = layers.Conv2D(nb_classes, (1, 1), activation='softmax')(decoder0)

    elif net_type == 11:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 32)
        encoder1_pool, encoder1 = encoder_block(encoder0_pool, 64)
        center = conv_block(encoder1_pool, 128)
        decoder1 = decoder_block(center, encoder1, 64)
        decoder0 = decoder_block(decoder1, encoder0, 32)
        outputs = layers.Conv2D(nb_classes,(1,1),activation='softmax')(decoder0)

    elif net_type == 12:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 16)
        encoder1_pool, encoder1 = encoder_block(encoder0_pool, 32)
        center = conv_block(encoder1_pool, 64)
        decoder1 = decoder_block(center, encoder1, 32)
        decoder0 = decoder_block(decoder1, encoder0, 16)
        outputs = layers.Conv2D(nb_classes,(1,1),activation='softmax')(decoder0)

    elif net_type == 13:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 32)
        center = conv_block(encoder0_pool, 256)
        decoder0 = decoder_block(center, encoder0, 32)
        outputs = layers.Conv2D(nb_classes, (1, 1), activation='softmax')(decoder0)

    elif net_type == 14:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 64)
        center = conv_block(encoder0_pool, 128)
        decoder0 = decoder_block(center, encoder0, 64)
        outputs = layers.Conv2D(nb_classes,(1,1),activation='softmax')(decoder0) 
        
    elif net_type == 15:
        inputs = layers.Input(shape=[img_size, img_size, 1])
        encoder0_pool, encoder0 = encoder_block(inputs, 32)
        center = conv_block(encoder0_pool, 64)
        decoder0 = decoder_block(center, encoder0, 32)
        outputs = layers.Conv2D(nb_classes,(1,1),activation='softmax')(decoder0)

    model = models.Model(inputs=inputs, outputs=outputs) 
    return model


def build_model(parameters, model_path):
    """
    This function builds/loads the selected model
    """

    print('Building model...')
    model = model_type(parameters)
    model.compile(optimizer='adam', loss = lf.param_cce_dice_loss(parameters), 
                 metrics=['accuracy', lf.param_categorical_acc(parameters), lf.param_dice_coeff(parameters), 
                 lf.param_iou_coef(parameters), lf.param_mean_iou(parameters)])
    model.summary()
    print('Model built.')

    if parameters['load_model']:
        print('Loading model weights...')
        model.load_weights(model_path)
        print('Weights loaded.')

    return model
