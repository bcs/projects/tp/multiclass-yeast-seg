from util_functions import load_data, evaluate_cell_segmentation, define_net_name, create_folders
from util_functions import save_history, experiment_data, save_exp_data
from train_cell_segmentation import train_cell_segmentation
from build_model import build_model
import numpy as np
import tensorflow as tf
import random as rn
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession
import os


# This is to avoid cuDNN initialization error and get convolution algorithm
config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

# for reproducible results
np.random.seed(2)
rn.seed(12)
tf.random.set_seed(2019)

# Parameters
train = 1                           # train the network if 1
test= 1                             # test the trained/loaded network if 1
parameters = {'gpu': 0,             # if using GPU
              'img_size': 128,      # size of input images
              'nb_classes': 3,      # number of classes
              'epochs': 1,          # number of epochs
              'batch_size': 8,      # batch size
              'load_model': 0,      # load pre-trained model if 1
              'net_type': 1,        # type of network model
              'new_class': 0,       # interface class created from weight map
              'w_loss': 0,          # weighted loss if 1
              'augmentation': 1,    # image augmentation (elastic deformation)
              'rotation': 0,        # angle to clockwise rotate images and orient trap position (fluid flows from top to bottom)
              'trap_threshold': 0.6 # threshold to detect and locate traps
              }

# name of the network if load_model = 1
net_name = "Net1_128_8b_3cl_nc1_wloss0"

net_name = define_net_name(parameters, net_name)    # change net name if load_model = 0
parameters['net_name'] = net_name                   # add net_name to parameters dictionary

# save configs
save = {'save_training_history': True,              # save plots of accuracy and loss
        'save_val_images': True,                    # save prediction images
        }

# define and create folders (save/load data)
folders = create_folders(parameters)  # folders can be changed in 'util_functions.py'

# Build/load the network model
if not parameters['gpu']:
    tf.config.optimizer.set_jit(True) # enable XLA

model = build_model(parameters, folders['model_path'])


# Read and pre-process dataset (Training and Validation images)
# functions and folders can be changed in util_functions.py
if train:
    print('----> Processing Training Data <----')
    [X_train, Y_train, W_train] = load_data(folders, parameters, 'train')   # Read Training Images
    print('----> Training Data Processed <----')
    
    print('----> Processing Validation Data <----')
    [X_val, Y_val, W_val] = load_data(folders, parameters, 'val')           # Read Validation Images
    print('----> Validation Data Processed <----')
    
    # Training
    history = train_cell_segmentation(X_train, Y_train, W_train, X_val, Y_val, W_val, model, net_name, parameters, save, folders)
    # make history_train.txt file with training information
    save_history(history.history, folders['save_dir'], net_name, "history_train.txt")


# Test network
if test:
    # reload best saved model
    if not parameters['gpu']:
        tf.config.optimizer.set_jit(True)  # enable XLA

    print('----> Processing Testing Data <----')
    [X_test, Y_test, W_test] = load_data(folders, parameters, 'test')  # Read Testing Images
    print('----> Testing Data Processed <----')

    parameters['load_model'] = 1
    model = build_model(parameters, folders['model_path'])
    
    eval_history, predictions = evaluate_cell_segmentation(X_test, Y_test, net_name, model, parameters, save, folders)
    # make history_test.txt file with testing information
    save_history(eval_history, folders['save_dir'], net_name, "history_test.txt")
