import tensorflow as tf
import tensorflow.keras as keras
import numpy as np
from tensorflow.python.keras import losses
from keras import backend as K
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import math_ops


def unstack_y_true(y_true, parameters):
    """
    check if the weight map is concatenated and unstack it from y_true
    """
    if parameters['w_loss']:
        weights = y_true[:,:,:,-1] # weights are the last dimention if present
        y_true = y_true[:,:,:,:-1]
    else:
        weights = []
    return y_true, weights
 
 
def set_new_class_as_background(y_true, y_pred):
    """
    make the new cell between cells as background
    """
    y_newclass = y_true[:,:,:,-1] # get new class labels
    y_background = y_true[:,:,:,0]
    new_background = y_newclass + y_background
    background = tf.expand_dims(new_background,-1)
    
    y_true = y_true[:,:,:,1:-1]    # unstack new class labels and background
    new_y_true = tf.concat([background, y_true], axis=-1)
    
    y_newclass_pred = y_pred[:,:,:,-1] # get new class labels
    y_background_pred = y_pred[:,:,:,0]
    new_background_pred = y_newclass_pred + y_background_pred
    background_pred = tf.expand_dims(new_background_pred,-1)
    
    y_pred = y_pred[:,:,:,1:-1]    # unstack new class labels and background
    new_y_pred = tf.concat([background_pred, y_pred], axis=-1)
    
    return new_y_true, new_y_pred

 
def param_iou_only_cell(parameters):
    def iou_only_cell(y_true, y_pred, smooth=1):
        """
        Intersection over Union coefficient only for cell class (IoU)
        IoU = (|X &amp; Y|)/ (|X or Y|)
        computes IoU for every class and average
        """
        # ignore weight map if present in last dimention of y_true
        y_true, _ = unstack_y_true(y_true, parameters)
        
        y_true = y_true[:,:,:,2:] # get cells masks
        y_pred = y_pred[:,:,:,2:] # get cells prediction
            
        intersection = K.sum(K.abs(y_true * y_pred), axis=[1,2])
        union = K.sum(y_true,[1,2])+K.sum(y_pred,[1,2])-intersection
        miou = K.mean((intersection + smooth) / (union + smooth), axis=1)
        return miou
    return iou_only_cell
    
def param_mean_iou(parameters):
    def mean_iou(y_true, y_pred, smooth=1):
        """
        mean Intersection over Union coefficient (IoU)
        IoU = (|X &amp; Y|)/ (|X or Y|)
        computes IoU for every class and average
        """
        # ignore weight map if present in last dimention of y_true
        y_true, _ = unstack_y_true(y_true, parameters)
        
        if parameters['nb_classes'] == 3:
            class_weights = [1, 1, 2]
        if parameters['nb_classes'] == 4:
            class_weights = [1, 1, 2, 2]
        intersection = K.sum(K.abs(y_true * y_pred), axis=[1,2])
        union = K.sum(y_true,[1,2])+K.sum(y_pred,[1,2])-intersection
        miou = 0
        for i in range(len(class_weights)):
            miou += class_weights[i]*(intersection[:,i] + smooth)/(union[:,i] + smooth)
        miou /= np.sum(class_weights)
        return miou
    return mean_iou


def param_iou_coef(parameters):
    def iou_coef(y_true, y_pred, smooth=1):
        """
        Intersection over Union coefficient (IoU)
        IoU = (|X &amp; Y|)/ (|X or Y|)
        """
        # ignore weight map if present in last dimention of y_true
        y_true, _ = unstack_y_true(y_true, parameters)
        
        intersection = K.sum(K.abs(y_true * y_pred), axis=[1,2,3])
        union = K.sum(y_true,[1,2,3])+K.sum(y_pred,[1,2,3])-intersection
        iou = K.mean((intersection + smooth) / (union + smooth), axis=0)
        return iou
    return iou_coef

def param_dice_coeff(parameters):
    def dice_coeff(y_true,y_pred):
        """
        Dice coefficient metric to compute the dice coefficient loss 
        Source: https://github.com/tensorflow/models/blob/master/samples/outreach/blogs/segmentation_blogpost/image_segmentation.ipynb
        """
        # ignore weight map if present in last dimention of y_true
        y_true, _ = unstack_y_true(y_true, parameters)
    
        smooth = 1.
        y_true_f = tf.reshape(y_true,[-1])
        y_pred_f = tf.reshape(y_pred,[-1])
        intersection = tf.reduce_sum(y_true_f * y_pred_f)
        score = (2. * intersection + smooth) / (tf.reduce_sum(y_true_f) + tf.reduce_sum(y_pred_f) + smooth)
        return score
    return dice_coeff


def param_dice_loss(parameters):
    def dice_loss(y_true , y_pred):
        """
        Dice coefficient loss function
        Paper: V-Net: Fully Convolutional Neural Networks for Volumetric Medical Image Segmentation
        Source: https://github.com/tensorflow/models/blob/master/samples/outreach/blogs/segmentation_blogpost/image_segmentation.ipynb
        """    
        # ignore weight map if present in last dimention of y_true
        y_true, _ = unstack_y_true(y_true, parameters)
        
        smooth = 1.
        y_true_f = tf.reshape(y_true,[-1])
        y_pred_f = tf.reshape(y_pred,[-1])
        intersection = tf.reduce_sum(y_true_f * y_pred_f)
        dice_coeff = (2. * intersection + smooth) / (tf.reduce_sum(y_true_f) + tf.reduce_sum(y_pred_f) + smooth)
        
        loss = 1 - dice_coeff
        return loss
    return dice_loss


def param_weighted_cross_entropy_loss(parameters):
    def weighted_cross_entropy_loss(y_true, y_pred):
        """
        This function is the weighted, categorical cross-entropy as used in U-Net paper
        (Important to detect the small borders between cells for segmentation)
        """
        # get weight map and y_true
        y_true, weights = unstack_y_true(y_true, parameters)
        
        epsilon = tf.convert_to_tensor(K.epsilon(), y_pred.dtype.base_dtype)
        y_pred = tf.clip_by_value(y_pred, epsilon, 1. - epsilon)
        y_log = tf.math.log(y_pred)
        y_mult = y_log*y_true
        loss = tf.reduce_sum(y_mult, -1)
        if parameters['w_loss']:
            loss = loss*weights
        loss = tf.math.reduce_mean(loss)
        return -loss
    return weighted_cross_entropy_loss
        
        
def param_cross_entropy_loss(parameters):
    def cross_entropy_loss(y_true, y_pred):
        """
        Categorical cross-entropy ignoring weight map in y_true
        """
        # ignore weight map if present in last dimention of y_true
        y_true, _ = unstack_y_true(y_true, parameters)
        
        return losses.categorical_crossentropy(y_true,y_pred)
    return cross_entropy_loss


def param_cce_dice_loss(parameters):
    
    def cce_dice_loss(y_true, y_pred):
        """
        Custom loss function
        Combined (weighted) cross-entropy loss and dice loss
        Source: https://github.com/tensorflow/models/blob/master/samples/outreach/blogs/segmentation_blogpost/image_segmentation.ipynb
        """
    
        # get weight map and y_true
        y_true, weights = unstack_y_true(y_true, parameters)
        
        # weighted categorical cross-entropy
        epsilon = tf.convert_to_tensor(K.epsilon(), y_pred.dtype.base_dtype)
        y_pred = tf.clip_by_value(y_pred, epsilon, 1. - epsilon)
        y_log = tf.math.log(y_pred)
        y_mult = y_log*y_true
        loss = tf.reduce_sum(y_mult, -1)
        if parameters['w_loss']:
            loss = loss*weights
        loss = tf.math.reduce_mean(loss)
        cce_loss = -loss
    
        # dice loss
        smooth = 1.
        y_true_f = tf.reshape(y_true,[-1])
        y_pred_f = tf.reshape(y_pred,[-1])
        intersection = tf.reduce_sum(y_true_f * y_pred_f)
        score = (2. * intersection + smooth) / (tf.reduce_sum(y_true_f) + tf.reduce_sum(y_pred_f) + smooth)
        dice = 1-score
        
        loss = cce_loss + dice
        return loss
    return cce_dice_loss

def param_tversky_loss(parameters):
    def tversky_loss(y_true, y_pred):
    
        """
        Ref: salehi17, "Twersky loss function for image segmentation using 3D FCDN"
        -> the score is computed for each class separately and then summed
        alpha=beta=0.5 : dice coefficient
        alpha=beta=1   : tanimoto coefficient (also known as jaccard)
        alpha+beta=1   : produces set of F*-scores
        implemented by E. Moebel, 06/04/18
        """
        alpha = 0.5
        beta  = 0.5
        
        # ignore weight map if present in last dimention of y_true
        y_true, _ = unstack_y_true(y_true, parameters)
        
        ones = K.ones(K.shape(y_true))
        p0 = y_pred      # proba that voxels are class i
        p1 = ones-y_pred # proba that voxels are not class i
        g0 = y_true
        g1 = ones-y_true
        
        num = K.sum(p0*g0, (0,1,2,3))
        den = num + alpha*K.sum(p0*g1,(0,1,2,3)) + beta*K.sum(p1*g0,(0,1,2,3))
        
        T = K.sum(num/den) # when summing over classes, T has dynamic range [0 Ncl]
        
        Ncl = K.cast(K.shape(y_true)[-1], 'float32')
        loss = Ncl-T
        return loss
    return tversky_loss


def param_F1_Score(parameters):
    def F1_Score(y_true, y_pred, smooth=1):
        """
        Dice coefficient - F1 Score 
        """
        # ignore weight map if present in last dimention of y_true
        y_true, _ = unstack_y_true(y_true, parameters)
        
        intersection = K.sum(y_true * y_pred, axis=[1,2,3])
        union = K.sum(y_true, axis=[1,2,3]) + K.sum(y_pred, axis=[1,2,3])
        dice = K.mean((2. * intersection + smooth)/(union + smooth), axis=0)
        return dice
    return F1_Score


def param_pixelwise_weighted_binary_crossentropy(parameters):
    def pixelwise_weighted_binary_crossentropy(y_true, y_pred):
        """
        This function implements pixel-wise weighted, categorical cross-entropy
        (Important to detect the small borders between cells for segmentation)
        
        The code is adapted from the Keras TF backend.
        (see their github)
        """
        try:
            # The weights are passed as part of the y_true tensor:
            seg = y_true[:,:,:,:-1]
            weight = y_true[:,:,:,-1]
            weight = tf.expand_dims(weight, -1)
        except:
            pass
    
        epsilon = tf.convert_to_tensor(K.epsilon(), y_pred.dtype.base_dtype)
        y_pred = tf.clip_by_value(y_pred, epsilon, 1. - epsilon)
        y_pred = tf.compat.v1.log(y_pred/(1 - y_pred))
    
        zeros = array_ops.zeros_like(y_pred, dtype=y_pred.dtype)
        cond = (y_pred >= zeros)
        relu_logits = math_ops.select(cond, y_pred, zeros)
        neg_abs_logits = math_ops.select(cond, -y_pred, y_pred)
        entropy = math_ops.add(relu_logits - y_pred * seg, math_ops.log1p(math_ops.exp(neg_abs_logits)), name=None)
        
        return K.mean(math_ops.multiply(weight, entropy), axis=-1)
    return pixelwise_weighted_binary_crossentropy

def param_categorical_acc(parameters):
    def categorical_acc(y_true, y_pred):
        """ 
        This function unstacks the mask from the weights in the output tensor for
        segmentation and performs categorical accuracy on the former
        """
        # ignore weight map if present in last dimention of y_true
        y_true, _ = unstack_y_true(y_true, parameters)
        
        return keras.metrics.categorical_accuracy(y_true, y_pred)
    return categorical_acc


def iou_loss_core(y_true, y_pred):  
    """
    Intersection over Union coefficient (IoU)
    IoU = (|X &amp; Y|)/ (|X or Y|)
    
    Obs: this can be used as a loss if you make it negative
    """
    
    intersection = y_true * y_pred
    notTrue = 1 - y_true
    union = y_true + (notTrue * y_pred)

    return (K.sum(intersection, axis=-1) + K.epsilon()) / (K.sum(union, axis=-1) + K.epsilon())

def castF(x):
    return K.cast(x, K.floatx())


def castB(x):
    return K.cast(x, bool)


def competitionMetric2(true, pred): 
    """
    Metrics implemented in Keras
    Source: https://www.kaggle.com/c/tgs-salt-identification-challenge/discussion/63044
    
    Any shape can go - can't be a loss function
    """
    
    tresholds = [0.5 + (i*.05)  for i in range(10)]

    # flattened images (batch, pixels)
    true = K.batch_flatten(true)
    pred = K.batch_flatten(pred)
    pred = castF(K.greater(pred, 0.5))

    # total white pixels - (batch,)
    trueSum = K.sum(true, axis=-1)
    predSum = K.sum(pred, axis=-1)

    # has mask or not per image - (batch,)
    true1 = castF(K.greater(trueSum, 1))    
    pred1 = castF(K.greater(predSum, 1))

    # to get images that have mask in both true and pred
    truePositiveMask = castB(true1 * pred1)

    # separating only the possible true positives to check iou
    testTrue = tf.boolean_mask(true, truePositiveMask)
    testPred = tf.boolean_mask(pred, truePositiveMask)

    # getting iou and threshold comparisons
    iou = iou_loss_core(testTrue,testPred) 
    truePositives = [castF(K.greater(iou, tres)) for tres in tresholds]

    # mean of thressholds for true positives and total sum
    truePositives = K.mean(K.stack(truePositives, axis=-1), axis=-1)
    truePositives = K.sum(truePositives)

    # to get images that don't have mask in both true and pred
    trueNegatives = (1-true1) * (1 - pred1) # = 1 -true1 - pred1 + true1*pred1
    trueNegatives = K.sum(trueNegatives) 

    return (truePositives + trueNegatives) / castF(K.shape(true)[0])
