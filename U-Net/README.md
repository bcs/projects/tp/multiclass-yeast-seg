# UNet - BCS Group
Repository with the U-Net code used in the BCS group to segment cells in a microfluidic device.

## Create Virtual Environment
First you need to create a virtual environment.

**Using anaconda**:

In the command prompt run:

```
conda create --name unet_bcs python=3.6
```

Then activate your environment:

```
activate unet_bcs
```

Now we need to install the packages inside our virtual environment.

## Install TensorFlow
The TensorFlow 2.0 will be installed separated from the *requirements.txt* because we can have GPU or CPU versions.

### GPU
If you have a GPU, run the following command:

```
pip install tensorflow-gpu==2.0.0-rc0
```

### CPU
If you have only CPU, use the command

```
pip install tensorflow==2.0.0-rc0
```

### Install Requirements
Install necessary packages by running the command

```
pip install -r requirements.txt
```


## Train a New Network

Change the parameters in *main.py* 
- set 'train' to 1
- set 'load_model' to 1 if you want to load previous weights
    - if you want to load a model, give the name of the .hfd5 file
    - this file should be inside "Networks" folder
- change batch size and number of epochs and other parameters according to your desire

```
# Parameters
train = 1                               # train the network if 1
val= 1                                  # validate the trained/loaded network if 1
test_exp = 0                            # to run the experiment as test
parameters = {  'gpu': 1,               # if using GPU
                'img_size': 128,        # size of input images
                'nb_classes': 3,        # number of classes
                'epochs': 30,           # number of epochs
                'batch_size': 8,        # batch size
                'load_model': 0,        # load pre-trained model if 1
                'net_type': 1,          # type of network model
                'new_class': 0,         # interface class created from weight map
                'w_loss': 0,            # weighted cross entropy loss
                'augmentation': 1,      # image augmentation (elastic deformation)
                'rotation': 0,          # angle to clockwise rotate images and orient trap position (fluid flows from top to bottom)
                'trap_threshold': 0.6   # threshold to detect and locate traps
              }

# name of the network if load_model = 1
net_name = "Net1_128_8b_3cl_nc1_wloss0"
```

then run the file *main.py*

```
python main.py
```

The dataset is read from the folder "*dataset*", which contains "*train*" and "*val*" folders.

The results will be saved in a folder called "*predicted labels*"

## Run Trained Network in Test Data

Change parameters in *main.py*

- set 'test' to 1
- set 'train' to 0
- set 'load_model' to 1
- write the name of the network to be tested in variable 'net_name'


then run the file *main.py*

```
python main.py
```

## Inference Tutorial

If you prefer, check how to run inferences on images in our [Jupyter Notebook Tutorial](U-Net/tutorial/unet_inference_tutorial.ipynb).
