import os
import matplotlib.pyplot as plt
import tensorflow as tf
import time
import numpy as np
import elasticdeform

"""
Elastic deformation from https://github.com/gvtulder/elasticdeform
"""

class TimeHistory(tf.keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, epoch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, epoch, logs={}):
        self.times.append(time.time() - self.epoch_time_start)

def train_cell_segmentation(X_train, Y_train, W_train, X_val, Y_val, W_val, model, net_name, parameters, save, folders):
    """
    This function trains the network model with the input dataset and
    saves the accuracy and loss curves if save['save_training_history'] = 1
    
    It returns the training history
    """
    
    # get parameters
    batch_size = parameters['batch_size']
    epochs = parameters['epochs']
    time_callback = TimeHistory()

    # make image augmentation (only elastic deformation)
    if parameters['augmentation']:
        [X_train_aug, Y_train_aug] = elasticdeform.deform_random_grid([X_train, Y_train], order=[3, 3], sigma=3, points=3, mode='nearest', prefilter=False,  axis = [(1,2), (1,2)])               
        X_train = np.concatenate((X_train, X_train_aug), axis=0)
        Y_train = np.concatenate((Y_train, Y_train_aug), axis=0)

    # model checkpoint
    cp = tf.keras.callbacks.ModelCheckpoint(filepath = folders['model_path'],
                                            monitor = 'val_dice_loss',
                                            save_best_only = True,
                                            save_weights_only = True,
                                            verbose = 1)
    
    # train the model
    print('----> Start training <----...')
    history = model.fit(X_train, 
                        Y_train, 
                        batch_size = batch_size,
                        epochs = epochs,
                        verbose = 1, 
                        validation_data = (X_val, Y_val),
                        callbacks = [cp, time_callback]
                        )
    
    history.history['time_epoch'] = time_callback.times
    history.history['num_train_samples'] = X_train.shape[0]
    
    # save accurcy and loss curves
    if save['save_training_history']:
        # get values
        loss = history.history['loss']
        val_loss = history.history['val_loss']
        epochs_range = range(epochs)

        # plot training/validation loss subfigure and save figure
        plt.plot(epochs_range,loss,label='Training Loss')
        plt.plot(epochs_range,val_loss,label='Validation Loss')
        plt.legend(loc='upper right')
        plt.title('Training and Validation Loss')
        plt.savefig(os.path.join(folders['save_dir'],'!summary_'+net_name))
        plt.close()
        
        
        # plot and save training/validation accuracy per epoch
        # gets accuracy name
        acc_name = [a for a in list(history.history.keys()) if "acc" in a]
        plt.figure(figsize=(16,8))
        plt.plot(history.history[acc_name[0]])
        plt.plot(history.history[acc_name[1]])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.savefig(os.path.join(folders['save_dir'],'!accuracy_'+net_name))
        plt.close()  
        
    return history

    
    

 
