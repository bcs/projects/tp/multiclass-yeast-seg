import numpy as np
import cv2

def pre_process(img, img_size, norm=False, scale=False, reshape=False, label=False):
    """
    Pre-processing
    
    Input
    -------------------------
    img: array image to be processed
    img_size: size for resize image (img_size, img_size)
    norm: if 1 computes image normalization. Applied to input images, not to labeled images
    scale: if 1 rescales the image between 0 and 1. Applied to input images, not to labeled images
    reshape: if 1 resize the image to shape (img_size, img_size, 1) -- channels last format
    label: if 1 the processed image is an output labeled image
    
    
    Pre-processing steps  
    -------------------------
            1) Resize
            2) Rescale (values between 0 and 1 -- divide by 255 for uint8 images)
            3) Normalization (standard normalization)
            4) Reshape (channel dimension for training the network - channels last format)
    """
    
    # if output image
    if label:
        # Resize for the labels (interpolation Nearest)
        img = cv2.resize(img, (img_size, img_size), interpolation=cv2.INTER_NEAREST)
        img = img.astype('int')     # labels [0, 1, ..., nb_classes-1]
    
    # if not output image
    else:
        img = cv2.resize(img, (img_size, img_size)) # default: INTER_LINEAR – a bilinear interpolation
        img = img.astype('float32') # float values to scale and normalize

    # rescale image
    if scale:           
        img /= 255 # scale for uint8
    
    # image normalization
    if norm:
        adjusted_std = max(np.std(img), 1.0/np.sqrt(img.size))
        img = (img-np.mean(img))/adjusted_std

    # reshape image
    if reshape:
        img = img.reshape(img_size, img_size, 1)
        
    return img
