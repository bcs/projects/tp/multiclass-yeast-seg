import numpy as np
import ntpath
import glob
import cv2
from PIL import Image
import os, pickle
from scipy.signal import fftconvolve
import tensorflow as tf
from pre_processing import pre_process
import matplotlib as mpl
import matplotlib.pyplot as plt
from keras.utils import np_utils
from skimage.measure import label, regionprops
from skimage.morphology import medial_axis, square, binary_opening, binary_erosion, watershed
from time import  process_time, time
from scipy import ndimage
import matplotlib.animation as animation
from matplotlib.animation import PillowWriter


class TimeHistory(tf.keras.callbacks.Callback):
    """
    Class to get training epoch time
    """
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, epoch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, epoch, logs={}):
        self.times.append(time.time() - self.epoch_time_start)


def create_folders(parameters, experiment_folder="Experiments"):
    """ 
    This function defines the path to load the dataset and network 
    and create the folders to save the results depending on the 'net_name'
    """ 
    net_name = parameters['net_name']
    train_input_path = os.path.join("dataset", "train", "input", "*.jpg")
    train_output_path = os.path.join("dataset", "train", "output-"+str(parameters['nb_classes'])+"-classes", "*.png")
    weights_train_path = os.path.join("dataset", "train", "wmap", "*.png")
    interface_weights_train_path = os.path.join("dataset", "train", "wmap_interface", "*.png")
    val_input_path = os.path.join("dataset", "val", "input", "*.jpg")
    val_output_path = os.path.join("dataset", "val", "output-"+str(parameters['nb_classes'])+"-classes", "*.png")
    weights_val_path = os.path.join("dataset", "val", "wmap", "*.png")
    interface_weights_val_path = os.path.join("dataset", "val", "wmap_interface", "*.png")
    test_input_path = os.path.join("dataset", "test", "input", "*.jpg")
    test_output_path = os.path.join("dataset", "test", "output-"+str(parameters['nb_classes'])+"-classes", "*.png")
    weights_test_path = os.path.join("dataset", "test", "wmap", "*.png")
    interface_weights_test_path = os.path.join("dataset", "test", "wmap_interface", "*.png")
    model_path = os.path.join(os.getcwd(), "Networks", net_name+".hdf5")        # path to save the model
    save_dir = os.path.join(os.getcwd(), "predicted_labels", net_name)          # path to save labels and results
    experiment_raw_files_path = os.path.join(experiment_folder, '*.tif')
    experiment_results_dir = os.path.join(experiment_folder, "Results", net_name)
    
    # create path to save results
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    if not os.path.exists(os.path.join(os.getcwd(), "Networks")):
        os.makedirs(os.path.join(os.getcwd(), "Networks"))
    
    if not os.path.exists(experiment_results_dir):
        os.makedirs(experiment_results_dir)

    
    # make folders dictionary
    folders = {'train_input_path': train_input_path,
               'train_output_path': train_output_path,
               'weights_train_path': weights_train_path,
               'interface_weights_train_path': interface_weights_train_path,
               'val_input_path': val_input_path,
               'val_output_path': val_output_path,
               'weights_val_path': weights_val_path,
               'interface_weights_val_path': interface_weights_val_path,
               'test_input_path': test_input_path,
               'test_output_path': test_output_path,
               'weights_test_path': weights_test_path,
               'interface_weights_test_path': interface_weights_test_path,
               'model_path' : model_path,
               'save_dir': save_dir,
               'experiment_raw_files_path': experiment_raw_files_path,
               'experiment_results_dir': experiment_results_dir
            }
    
    return folders


def define_net_name(parameters, net_name):
    """
    This function defined the net_name if you don't want to load a previous trained model
    It contains the parameters used during training the network
    format: [netType, input_size, batch_size, nb_epochs, nb_classes, newClass]
    Ex: Net1_128_2b_10ep_4cl_nc1
    """
    
    if not parameters['load_model']:    
        net_name = "Net"+str(parameters['net_type'])+"_"+str(parameters['img_size'])+"_"+str(parameters['batch_size'])+"b_"+str(parameters['epochs'])+"ep_"\
        +str(parameters['nb_classes'])+"cl_nc"+str(parameters['new_class'])+'_wloss'+str(parameters['w_loss'])
    return net_name


def load_data(folders, parameters, data_split):
    """
    This function loads the dataset from 'input_path' and 'output_path'
    
    Returns
    ----------------------    
    Numpy array X of shape (nImages, img_size, img_size, 1) and
    Numpy array Y of shape (nImages, img_size, img_size, nb_classes) 
                        or (nImages, img_size, img_size, nb_classes + 1) if parameters['new_class'] = 1
    Numpy array W of shape (nImages, img_size, img_size) 
    
    """
    if data_split == 'train':
        input_path = folders['train_input_path']
        output_path = folders['train_output_path']
        weights_path = folders['weights_train_path']
        interface_weights_path = folders['interface_weights_train_path']
    
    elif data_split == 'val':
        input_path = folders['val_input_path']
        output_path = folders['val_output_path']
        weights_path = folders['weights_val_path']
        interface_weights_path = folders['interface_weights_val_path']

    elif data_split == 'test':
        input_path = folders['test_input_path']
        output_path = folders['test_output_path']
        weights_path = folders['weights_test_path']
        interface_weights_path = folders['interface_weights_test_path']

    # update nb_classes for the one hot encoding if 'new_class' (interface class)
    if parameters['new_class']:
        nb_classes = parameters['nb_classes'] + 1  
    else:
        nb_classes = parameters['nb_classes']

    X = []
    Y = []
    W = []      # not used now for the new interface class, but can be used for weighted cross-entropy loss

    input_files = glob.glob(input_path)
    output_files = glob.glob(output_path)
    weights_files = glob.glob(weights_path)
    interface_weights_files = glob.glob(interface_weights_path)
    
    # sort files
    input_files = sorted(input_files)             
    output_files = sorted(output_files)           
    weights_files = sorted(weights_files)
    interface_weights_files = sorted(interface_weights_files)
    
    # load and pre-process dataset
    nb_files = len(input_files) # input/output/wmaps must have same length
    for idx in range(nb_files):                    
        # load and pre-process input data
        x = cv2.imread(input_files[idx], -1)  # loads input image as such including alpha channel (-1) or (0) as gray scale
        x = pre_process(x, parameters['img_size'], norm = True, scale = True, reshape = True) # see pre_process.py
        X.append(x)

        # load and pre-process output data (labels)
        y = Image.open(output_files[idx])
        y = np.array(y)
        y = pre_process(y, parameters['img_size'], label = True)  # just resize in this pre-processing
        
        # load pre-computed weight map
        if parameters['w_loss']:
            w = cv2.imread(weights_files[idx], 0)
            w = pre_process(w, parameters['img_size'], scale = True)
            W.append(w)
        
        # load pre-computed weight map for interface class
        if parameters['new_class']:
            w_interface = cv2.imread(interface_weights_files[idx], 0)
            w_interface = pre_process(w_interface, parameters['img_size'], scale = True)
            new_class = (w_interface>0.11765).astype(int)  # make binary mask for the new class label (back value 30/255 = 0.117647)
            # create and add new class label
            new_class = (nb_classes-1)*new_class
            y = np.multiply(y, new_class[:,:]==0) # set intersection to background
            y = y + new_class
        
        # one hot encoding
        y = np_utils.to_categorical(y, nb_classes) 
        Y.append(y)
        
    # list to array
    X = np.array(X)
    Y = np.array(Y)
    W = np.array(W)
    
    # Concatenate to apply weighted cross-entropy loss implemented in Keras
    # Will be separated before loss computation    
    if parameters['w_loss']:
        W = W[:,:,:,np.newaxis]
        Y = np.concatenate((Y, W), axis=-1)
        
    return X, Y, W

def rotate_image(img, angle):
    """
    This function rotates clockwise the image 'img' when the experiment has a different orientation from the trained dataset.
    It should be rotated such that the fluid flows from top to bottom.
    
    'angle': angle to clockwise rotate images and orient traps 
    """
    if angle == 90:
        # Rotate 90
        img = cv2.rotate(img, rotateCode=cv2.ROTATE_90_CLOCKWISE)
    elif angle == 180:
        # Rotate 180
        img = cv2.rotate(img, rotateCode=cv2.ROTATE_180)
    elif angle == -90:
        # Rotate 90 counter-clockwise
        img = cv2.rotate(img, rotateCode=cv2.ROTATE_90_COUNTERCLOCKWISE)   
    return img


def weightmap(mask, classweights, nb_classes, w0 = 50, sigma = 50):
    """
    This function computes the weight map as described in the original U-Net
    paper to force the model to learn borders in new class
    (Used to preprocess the weight maps - too slow to use as pre-processing step)
    Source: https://gitlab.com/dunloplab/delta/blob/master/data.py
    """  
    lblimg,lblnb = label(mask[:,:],connectivity=1,return_num=True)
    distance_array = float('inf')*np.ones(mask.shape[0:2] + (max(lblnb,2),))
    for i in range(0,lblnb):
        [_,distance_array[:,:,i]] = medial_axis((lblimg==i+1)==0,return_distance=True)
    distance_array = np.sort(distance_array,axis=-1)
    weightmap = w0*np.exp(-np.square(np.sum(distance_array[:,:,0:2],-1))/(2*sigma^2))
    weightmap = np.multiply(weightmap, mask[:,:]==0)
    for cl in range(nb_classes):
        weightmap = np.add(weightmap, (mask[:,:]==cl)*classweights[cl])
    return weightmap
    

def predict_mask(predictions, new_class=False):
    """
    This function generates the masks from the predictions
    it uses a threshold of 0.9 to identify the pixels class
    """
    nb_class = predictions.shape[-1]                        # number of classes is the last dimension
    masks = np.copy(predictions)
    for idx in range(nb_class):
        if idx == nb_class-1 and new_class:
            masks[:,:,:,idx] = 0*(masks[:,:,:,idx]>0.9)     # labeled prediction        # set new class to background
        else:
            masks[:,:,:,idx] = idx*(masks[:,:,:,idx]>0.9)   # labeled prediction
    masks = np.sum(masks, axis=-1)
    return masks


def cell_masks(predictions, parameters):
    """
    This function generates the cells mask from predictions.
    It uses a threshold of 0.9 to identify the pixel class
    """
    masks = np.copy(predictions)
    if parameters['nb_classes'] == 3:
        masks = (masks[:,:,:,2]>0.9).astype(int)  # get cell predictions
    elif parameters['nb_classes'] ==4:
        masks = (masks[:,:,:,2]>0.9).astype(int)  # get target cell predictions
    return masks


def my_cmap(nb_classes):
    """
    This function defines a custom colormap for the labeled images
    """
    if nb_classes >4:
        cmap =  mpl.colors.ListedColormap(['white', 'darkgreen', 'red', 'blue', 'orange', 'magenta', 'plum']) # used for watershed
    if nb_classes == 4:
        cmap =  mpl.colors.ListedColormap(['white', 'black', 'darkgreen', 'yellowgreen']) # background, trap, secondary cells and target cells
    elif nb_classes == 3:
        cmap =  mpl.colors.ListedColormap(['white', 'black', 'darkgreen'])  # background, trap and cell
    elif nb_classes == 2:
        cmap =  mpl.colors.ListedColormap(['white', 'black'])               # only background and trap
    else:
        cmap = mpl.colors.ListedColormap(['white'])                         # only background
    return cmap
    

def postprocess(predictions, parameters, squareSize=5):
    """ 
    The post-process binarizes the cells prediction and applies the opening mathematical morphology
    'squareSize' is the size os the square used to apply 'opening'
    """
    cells_pred = cell_masks(predictions, parameters)    # get cells masks
    nb_imgs = predictions.shape[0]                      # number of predicted images
    for idx in range(nb_imgs):
        cells_pred[idx,:,:] = binary_opening(cells_pred[idx,:,:], selem=square(squareSize)) # apply binary opening
    return cells_pred    


def save_image_labels(original_image, predicted_mask, image_name, save_dir, net_name):
    """
    This function saves the results if save['save_val_images'] = 1
    """
    nb_classes = predicted_mask.max()
    cmap = my_cmap(nb_classes+1)
    
    plt.figure(figsize=(12,6))
    ax1 = plt.subplot(1, 3, 1)
    ax1.imshow(original_image, 'gray')
    plt.xticks([])
    plt.yticks([])
    plt.title("Input image")
    
    ax3 = plt.subplot(1, 3, 2)
    ax3.imshow(original_image, 'gray')
    ax3.imshow(predicted_mask, alpha = 0.4, cmap = cmap)
    plt.xticks([])
    plt.yticks([])
    plt.title("Labeloverlay")

    ax4 = plt.subplot(1, 3, 3)    
    ax4.imshow(predicted_mask, cmap = cmap)
    plt.xticks([])
    plt.yticks([])
    plt.title("Predicted Mask")
    
    plt.suptitle(net_name+'\n'+image_name, size = 16)
    plt.tight_layout()
    plt.subplots_adjust(top=0.88)
    plt.savefig(os.path.join(save_dir, image_name+'_predicted'))
    plt.close()
    
    return 


def cross_image(im1, im2):
    """
    This function does the cross-correlation 
    Used to correct the shift between images
    """
    
    # transform the type cast into 'float' to avoid overflows
    im1 = im1.astype('float32')
    im2 = im2.astype('float32')

    # get rid of the averages to improve results
    im1 -= np.mean(im1)
    im2 -= np.mean(im2)
    
    #  calculate the correlation image
    return fftconvolve(im1, im2[::-1, ::-1], mode='same')
    
    
def pixels_to_shift(Reference, big_image):
    """
    This function returns the number of pixels for the alignment correction between images in the experiment  
    """
    # get a small region of images and apply cross correlation
    im1 = np.copy(big_image[:750,:750])
    im2= np.copy(Reference[:750, :750])
    cc_img = cross_image(im1, im2)
    
    # get brightest spot coordinates
    x_center , y_center = np.unravel_index(np.argmax(cc_img), cc_img.shape)
    x_alignment = im1.shape[0]/2 - x_center   
    y_alignment = im1.shape[0]/2 - y_center
    return (x_alignment, y_alignment)


def shift_image(big_image, alignment_pixels):
    """
    This function returns the aligned shifted image  
    
    ***** Unused in the moment *****
    """
    x_alignment, y_alignment = alignment_pixels
    if x_alignment!=0 and y_alignment!=0:
        M = np.float32([[1, 0, x_alignment], [0, 1, y_alignment]])
        big_image = cv2.warpAffine(big_image, M, big_image.shape)  # shifted big image
    return big_image    

    
def trap_positions(big_image, parameters):
    """
    This function returns the coordinates of the traps given the BF image and the shape of the template used
    The coordinates indicate the traps position
    Implemented by Jan Basrawi
    """
    threshold = parameters['trap_threshold']
    Y  = []
    X  = []
    max_match = 0
    templates_path  = os.path.join("templates","*.tif")
    heightMax, widthMax = big_image.shape
    for template_file in glob.glob(templates_path):
        template = cv2.imread(template_file, 0)  # -1 (uint16) or  0 (gray scale)
        height, width = template.shape
        match_output_img = cv2.matchTemplate(big_image, template, cv2.TM_CCOEFF_NORMED) # performing template match
        if np.max(match_output_img) > max_match:
            max_match= np.max(match_output_img)
        positions = np.where(match_output_img >= threshold) # find positions in the image where the values are >= threshold
        for point in zip(*positions[::-1]):
            (y,x) = point
            counter=0
            final_counter = 0
            y = y - 20
            x = x - 90
            if y >= 0 and x >= 0 and (x + height + 100) <= heightMax and (y + width + 50) <= widthMax: # verify if is inside of the big image
                if Y==[]:
                    Y.append(y)
                    X.append(x)
                for i in range(len(Y)):
                    same_counter = 0
                    if abs(y - Y[i]) >= 0 and abs(y - Y[i]) < width :
                        counter += 1
                        same_counter += 1
                    if abs(x - X[i]) >= 0 and abs(x - X[i]) < height:
                        counter += 1
                        if same_counter == 1:
                            same_counter += 1
                    if same_counter == 2:
                        final_counter += 1

                if counter != 2*len(Y) and same_counter != 2 and final_counter == 0 and y + width <= widthMax:
                    Y.append(y)
                    X.append(x)

    if len(Y) != 0:
        correction_pixels = [0]
        X, Y = zip(*sorted(zip(X, Y)))
        X, Y = (list(t) for t in zip(*sorted(zip(X, Y))))
        Z = list(zip(Y, X))
        for i in range(len(X) - 1):
            if X[i + 1] - X[i] <= 20:
                correction_pixels.append(X[i + 1] - X[i])
                X[i + 1] = X[i]
            if X[i + 1] - X[i] > 20:
                correction_pixels.append(0)
        X, Y = zip(*sorted(zip(X, Y)))
        X, Y = (list(t) for t in zip(*sorted(zip(X, Y))))
        for i in range(len(Y)):
            for j in range(len(Z)):
                if Y[i] == Z[j][0] and abs(Z[j][1] - X[i]) <= 20:
                    X[i] = X[i] + correction_pixels[j]
    return X, Y, template.shape


def trap_images(big_image, X, Y, template_shape, img_size, channel):
    """
    This function returns the small trap images given the coordinates of the traps
    """
    
    height, width = template_shape
    trap_imgs = []

    # select trap region and pre-process
    for i in range(len(X)):
       rect_x = X[i] + height + 100 
       rect_y = Y[i] + width + 50 
       trap_img = big_image[X[i]:rect_x, Y[i]:rect_y]
       
       if np.any(trap_img):
           # pre-process only if brightfield channel
           if channel == 'BF':
                   trap_img = pre_process(trap_img, img_size, norm=True, scale=True, reshape=True) # pre-processing step
       else:
           if channel == 'BF':
               trap_img = np.zeros((img_size,img_size,1))   # size reshaped as pre-processed
           else:
                trap_img = np.zeros((height + 100, width + 50)) 
       trap_imgs.append(trap_img)
    return trap_imgs


def make_rect_traps(big_image, X, Y, template_shape):
    """
    This function draws a rectangle where the traps are located in the image
    """
    rect_img = big_image.copy() 
    height, width = template_shape
    for i in range(len(X)):
       rect_x = X[i] + height + 100
       rect_y = Y[i] + width + 50
       cv2.rectangle(rect_img, (Y[i], X[i]), (rect_y, rect_x), (0, 0, 255), 2)  # draw rectangle around traps
       cv2.putText(rect_img, str(i+1), (Y[i]+5, X[i]+35), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0,0,255), 2) # write trap ID
    return rect_img


def evaluate_cell_segmentation(X_val, Y_val, net_name, model, parameters, save, folders):
    """
    This function evaluates the model using the validation dataset
    """
    
    # evaluate model
    print('Evaluating model')
    history_values = model.evaluate(X_val, Y_val, verbose=2)
    print('Model evaluated')
    
    # make history dictionary
    history = {}
    metrics_names = model.metrics_names
    for idx in range(len(history_values)):
        history[metrics_names[idx]] = history_values[idx]
    
    num_val_examples = len(X_val)

    # prediction of the labels
    print('Making predictions')
    start = process_time()
    predictions = model.predict(X_val, batch_size=1, verbose=1) 
    end = process_time()
    
    # history with number of examples and runtime
    history['number_of_samples'] = num_val_examples
    history['prediction_time'] = end-start
    
    # save results
    if save['save_val_images']:
        print('--- Saving prediction results ---')
        predicted_mask = predict_mask(predictions, parameters['new_class']) # make predictions mask
        for i in range(num_val_examples):
            image_name = ntpath.basename(glob.glob(folders['val_input_path'])[i])
            image_name = image_name[0:len(image_name)-4]
            save_image_labels(X_val[i,:,:,0], predicted_mask[i,:,:], image_name, folders['save_dir'], net_name)    # save validation results
    return history, predictions


def plot_fluorescence(data, num_z, experiment_folder, results_dir):    
    """
    This function plots the fluorescence values over time
    """
    
    trap_id = data['trap_id']
    time_point = data['time_point']
    fluorescence_value = data['fluorescence_value']
    background_fluorescence = data['background_fluorescence']
    Z = data['Z']
    for z in range(num_z):
        z_idx = np.where(Z == z)
        plt.figure(figsize=(18, 18))
        for trap in range(1,trap_id.max()+1):
            trap_idx = np.where(trap_id[z_idx] == trap)
            y_fluorescence = fluorescence_value[trap_idx]  
            y_background = background_fluorescence[trap_idx]
            y = y_fluorescence - y_background
            x = time_point[trap_idx]
            plt.plot(x,y,'o-',label = "Trap "+str(trap),markersize=2)
            for i in range(0,len(x),10):
                plt.annotate(trap, (x[i], y[i]))
            plt.annotate(trap, (x[-1], y[-1]))
        plt.title(experiment_folder+" - Z"+str(z))
        plt.legend(loc='center left', prop={'size': 10}, bbox_to_anchor=(1, 0.5))
        plt.xlabel('timepoint')
        plt.ylabel('fluorescence')
        plt.grid()
        plt.savefig(os.path.join(results_dir,"fluorescence_plot_Z"+str(z)))
        plt.close()
    return
    


def plot_area(data, num_z, experiment_folder, results_dir):    
    """
    This function plots the area values over time
    """
    
    trap_id = data['trap_id']
    time_point = data['time_point']
    area = data['area']
    Z = data['Z']
    for z in range(num_z):
        z_idx = np.where(Z == z)
        plt.figure(figsize=(18, 18))
        for trap in range(1,trap_id.max()+1):
            trap_idx = np.where(trap_id[z_idx] == trap)
            y = area[trap_idx]
            x = time_point[trap_idx]
            plt.plot(x,y,'o-',label = "Trap "+str(trap),markersize=2)
            for i in range(0,len(x),10):
                plt.annotate(trap, (x[i], y[i]))
            plt.annotate(trap, (x[-1], y[-1]))
                
        plt.title(experiment_folder+" - Z"+str(z))
        plt.legend(loc='center left', prop={'size': 10}, bbox_to_anchor=(1, 0.5))
        plt.xlabel('timepoint')
        plt.ylabel('area')
        plt.grid()
        plt.savefig(os.path.join(results_dir,"area_plot_Z"+str(z)))
        plt.close()
    return


def plot_fluorescence_area(data, num_z, experiment_folder, results_dir):
    """
    This function plots the fluorescence/area plots over time
    """
    
    fluorescence_area = data['fluorescence_area']
    trap_id = data['trap_id']
    time_point = data['time_point']
    Z = data['Z']    
    for z in range(num_z):
        z_idx = np.where(Z == z)
        plt.figure(figsize=(18, 18))
        for trap in range(1,trap_id.max()+1):
            trap_idx = np.where(trap_id[z_idx] == trap)
            y = fluorescence_area[trap_idx]
            x = time_point[trap_idx]
            plt.plot(x,y,'o-',label = "Trap "+str(trap),markersize=2)
            for i in range(0,len(x),10):
                plt.annotate(trap, (x[i], y[i]))
            plt.annotate(trap, (x[-1], y[-1]))
                
        plt.title(experiment_folder+" - Z"+str(z))
        plt.legend(loc='center left', prop={'size': 10}, bbox_to_anchor=(1, 0.5))
        plt.xlabel('timepoint')
        plt.ylabel('fluorescence/area')
        plt.grid()
        plt.savefig(os.path.join(results_dir,"fluorescence_area_plot_Z"+str(z)))
        plt.close()
    return


def save_history(history, save_dir, net_name, file_name):
    """
    This function saves the history (training or validation) as a .txt file according to the 'file_name'
    """


    data_file = open(os.path.join(save_dir,file_name),"w")
    data_file.write(net_name+"\n")      # write net name
    data_file.write("\n")
    
    # write string 'key' = 'value' for all keys in history dictionary
    for key in history.keys():                                            
        dt = key + ' = ' + str(history[key]) + '\n'       
        data_file.write(dt)
    data_file.close()
    return

    
def get_cells_info(big_image, img_GFP, X_ref, Y_ref, template_shape, time_point, model, parameters, cells_data):
    """
    This function calculates the fluorescence of the big image and store it as a list with important information
    
    Returns
    ------------------------------------------
    trap_images: Numpy array with the small trap images
    fluorescent_imgs: Numpy array with the small trap images in fluorescent channel
    cell_pred_mask: Numpy array with the predicted cells
    data: list with [timepoint, z_plane, trap_number, X_trap_coordinate, Y_trap_coordinate, X_alignment, Y_alignment, area_prediction, fluorescence]
    """
    
    img_size = parameters['img_size']
    # get traps from big image
    trap_imgs = trap_images(big_image, X_ref, Y_ref, template_shape, img_size, channel='BF')         # return shopped trap images
    fluorescent_imgs = trap_images(img_GFP, X_ref, Y_ref, template_shape, img_size, channel='GFP')   # return shopped fluorescent images
    num_test_examples = len(trap_imgs)  # number of traps in the big image

    # prediction of the labels
    predictions = model.predict(np.array(trap_imgs), batch_size=1, verbose=1)

    # postprocessing the predictions
    cell_pred_masks = postprocess(predictions, parameters)   
    
    # calculate fluorescence(channel001) for every trap image
    for trap_number in range(num_test_examples):
        # postprocess to calculate fluorescent for every cell in predicted cell mask
        cells_mask = cell_pred_masks[trap_number,:,:]
        fluorescent_img = fluorescent_imgs[trap_number]
        trap_shape =  fluorescent_img.shape # used to resize predicted mask and multiply by fluorescent image
        cells_data[0]['trap_shape'] = trap_shape

        if time_point == 0:
            cell_id = 1             # first cell id
        if cells_mask.max() > 0:    # check if it is not an empty trap (avoid nan in distance transform)

            # method 1: erosion -> label (connect components) -> watershed
            markers = binary_erosion(cells_mask, selem=square(5))   # apply erosion to separate touching cells and eliminate small regions
            markers, _ = ndimage.label(markers)                     # label cells with different values
            cells_mask = watershed(cells_mask, markers, mask=cells_mask) # apply watershed with markers and mask
            cells_mask = cv2.resize(cells_mask, trap_shape[::-1], interpolation=cv2.INTER_NEAREST)   # resize cells_mask to multiply by fluorescent image
            regions = regionprops(cells_mask)    # cells positions and properties
            
            """
            # method 2: dist_ranf_edt -> threshold -> label -> watershed
            distance_transform = ndimage.distance_transform_edt(cells_mask)
            distance_transform = distance_transform/distance_transform.max()
            
            dt_threshold = 0.25
            markers = (distance_transform > dt_threshold).astype(int)
            """
            
            # calculate background fluorescence by using background class
            back_fluorescence = np.multiply(cells_mask==0, fluorescent_img)
            back_fluorescence = np.mean(back_fluorescence)
            
            # information of every single cell in cells_mask after watershed or all time points
            for cell in regions:
                cells= {}
                cells['timepoint'] = time_point
                cells['trap_id'] = trap_number+1
                cells['cell_area'] =  cell.area
                cells['centroid'] = cell.centroid
                cells['coordinates'] = (cell.coords).astype('int')
                cells['bounding_box'] = cell.bbox
                cells['trap_shape'] = trap_shape
                cells['background_fluorescence'] = back_fluorescence

                # calculate fluorescence
                cell_fluorescence_img = np.multiply(cells_mask==cell.label, fluorescent_img)
                cell_fluorescence = sum(sum(cell_fluorescence_img)) - back_fluorescence 
                cells['fluorescence'] = cell_fluorescence

                # only for first time point: defined cell_ids
                if time_point == 0: 
                    cells['cell_id'] = cell_id  
                    cells_data[1][trap_number+1] = cell_id # initialize max cell id in timepoint 0
                    cells_data[0]['max_cell_id'][trap_number+1] = cell_id
                    cell_id += 1    # increment cell id for timepoint 0

                else:
                    cell_id = track_cell_id(cells_data, cell, time_point, trap_number+1)
                    
                    # check if cell_id was already labeled and remove from cells_data (means that one cell was divided as 2 cells after watershed)
                    cells_same_id = []
                    trap_cells = get_dict_elements('trap_id', trap_number+1, cells_data) # get from same trap
                    for cell_same in trap_cells:
                        if cell_same.get('timepoint') == time_point and cell_same.get('cell_id') == cell_id:
                            cells_same_id.append(cell_same)
                            cells_data.remove(cell_same) # remove from cells_data list
                    if len(cells_same_id) != 0:
                        centroid = [cell.centroid]
                        fluorescence = [cell_fluorescence]
                        area = [cell.area]
                        coords = (cell.coords).astype('int')
                        for cell_same_id in cells_same_id:
                            centroid.append(cell_same_id['centroid'])
                            fluorescence.append(cell_same_id['fluorescence'])
                            area.append(cell_same_id['cell_area'])
                            coords = np.concatenate((coords, cell_same_id['coordinates']), axis=0)
                        new_area = np.mean(area)
                        new_fluorescence = np.mean(fluorescence)
                        new_centroid=tuple([sum(y) / len(y) for y in zip(*centroid)])
                        cells['cell_area'] =  new_area
                        cells['centroid'] = new_centroid
                        cells['coordinates'] = coords
                        cells['fluorescence'] = new_fluorescence

                    cells['cell_id'] = cell_id
                    # update max_cell_id if there is a new cell id
                    if cell_id > cells_data[0]['max_cell_id'][trap_number+1]:
                        cells_data[0]['max_cell_id'][trap_number+1] = cell_id
                        
                # append dictionary with cells information to list
                cells_data.append(cells)
        # empty trap case
        else:   
            cells= {}
            cells['cell_id'] = 0 
            cells['timepoint'] = time_point
            cells['trap_id'] = trap_number+1
            cells['cell_area'] = 0
            cells['fluorescence'] = 0
            if time_point == 0:
                cells_data[1][trap_number+1] = 0 # initialize max cell id in timepoint 0
                cells_data[0]['max_cell_id'][trap_number+1] = 0
            cells_data.append(cells)
            
    return cells_data, trap_imgs, cell_pred_masks, predictions


def track_cell_id(cells_data, cell, time_point, trap_number):
    """
    Returns the id of the previous cell with closest centroid position based on Euclidean distance
    cell_centroid: tuple with current cell centroid
    cells: dictionary elements to get centroids and ids lists
    """

    cell_centroid = cell.centroid   # get cell centroid
    max_tp_iteration = 5            # number of previous timepoints fo look for cell id
    previous_timepoint = time_point - 1 # decrement previous timepoint
    tp_iteration = 1                    # timepoint iteration
    trap_cells = get_dict_elements('trap_id', trap_number, cells_data) # get from same trap

    # check distance to the 5 last timepoints if distance is big
    while previous_timepoint >= 0 and tp_iteration < max_tp_iteration:
        previous_timepoint_cells = get_dict_elements('timepoint', previous_timepoint, trap_cells) # get previous timepoints
        centroids, ids = get_centroids_and_ids(previous_timepoint_cells)
        distances = []
        if np.any(centroids): # if centroids is not None
            for centroid in centroids:
                distances.append(np.linalg.norm(centroid- cell_centroid)) # Euclidean distance for all centroid in centroids
    
            # check if the distance is small
            if min(distances) < 5:
                idx_min = distances.index(min(distances)) 
                cell_id = ids[idx_min]
                return cell_id   # found one previous prediction close to current cell
            
            # check if it was empty trap or prediction area is small ()
            if min(ids) == 0 or cell.area < 90:
                cell_id = 0
                return cell_id

            # else check area of all others

        previous_timepoint -= 1
        tp_iteration += 1
        
    # Here means that the cell is not close to the last 5 timepoints (assuming predictions)
    # Then create a new label
    cell_id = cells_data[0]['max_cell_id'][trap_number] + 1 # create new cell id (increment one)
    return cell_id
        

def get_centroids_and_ids(data):
    """
    return list of centroids and respective ids of data elements (list of dictionaries)
    """

    centroids = []
    ids = []
    for element in data:
        centroids.append(element.get('centroid'))
        ids.append(element.get('cell_id'))
    return np.array(centroids), np.array(ids)


def get_dict_elements(key, value, data):
    """
    return elements in data (list of dictionaries) that have the 'value' in 'key'
    """

    elements = []
    for element in data:
        if element.get(key) == value: 
            elements.append(element)
    return elements
 
    
def experiment_data(selected_zs, model, net_name, parameters, save, folders):    
    """
    This function reads the experiment and generates the data for all images of experiment
    It should be changed if the experiment images are not saved as 'img_channel000_positionxxx_timexxxxxxxxx_z00x'    
    """
    
    raw_files_path = folders['experiment_raw_files_path']
    raw_files = glob.glob(raw_files_path)
    
    # files for brightfield and fluorescent images
    files_ch0 = [file for file in raw_files if "channel000" in file]
    files_ch1 = [file for file in raw_files if "channel001" in file]
    
    exp_data = {}
    for z in selected_zs:
        # z-files for brightfield and fluorescent channels
        z_ch0 = [file for file in files_ch0 if "z00"+str(z) in file]
        z_ch1 = [file for file in files_ch1 if "z00"+str(z) in file]
  
        # data list of dictionaries initialization
        number_timepoints = len(z_ch0)
        data = []
        data.append({'max_timepoints': number_timepoints})  # time_points is the first element of the list
        data[0]['big_image_data'] = []
        data[0]['max_cell_id'] = {}
        data[0]['max_cell_id'][0] = 0

        for time_point in range(number_timepoints):
            if time_point == 0:     # read reference big image (timepoint 0)
                # get info data for reference image
                Reference = cv2.imread(z_ch0[time_point],  0)  
                Reference = rotate_image(Reference, parameters['rotation']) # rotate images if necessary
                alignment_pixels = (0,0)
                big_image = Reference
                X_ref, Y_ref, template_shape = trap_positions(big_image, parameters)   # return position of the traps of the big_image image
                num_traps = len(X_ref)
                data[0]['num_traps'] = num_traps # append number of detected traps in first position dictionary
                data[0]['template_shape'] = template_shape
            else:                
                # get info data for other timepoints image
                big_image = cv2.imread(z_ch0[time_point],  0) 
                big_image = rotate_image(big_image, parameters['rotation']) # rotate images if necessary
                alignment_pixels = pixels_to_shift(Reference, big_image)
                
            # subtract alignment for all the elements of list
            X_aligned = [int(x - alignment_pixels[0]) for x in X_ref]      
            Y_aligned = [int(y - alignment_pixels[1]) for y in Y_ref]
    
            image_name = ntpath.basename(z_ch0[time_point])[:-4]
            # read fluorescent channel
            img_GFP = cv2.imread(z_ch1[time_point], -1) # uint16 (-1)
            img_GFP = rotate_image(img_GFP, parameters['rotation']) # rotate images if necessary

            img_GFP = img_GFP.astype(np.float32)
                                    
            # initialize big image data
            big_images_data={}
            big_image_data = {}
            big_image_data['template_shape'] = template_shape
            big_image_data['alignment_pixels'] = alignment_pixels
            big_image_data['path'] = z_ch0[time_point]
            big_image_data['name'] = image_name
            big_image_data['z_plane'] = z
            big_image_data['X_aligned'] = X_aligned
            big_image_data['y_aligned'] = Y_aligned
            big_images_data[time_point] = big_image_data

            # second element of the list
            max_cell_id= {}     # list with max id of cell for every trap
            max_cell_id[0] = 0  # initialize max_cell_id
            data.append(max_cell_id)
            
            # save detected traps  (only timepoint = 0, 1 and 2)
            if save['save_rect_images'] and time_point < 3:   # save big images with rectangle
                rect_img = make_rect_traps(big_image, X_aligned, Y_aligned, template_shape)    
                save_dir = os.path.join(folders['experiment_results_dir'], "Traps_location")
                if not os.path.exists(save_dir):
                    os.makedirs(save_dir)
                cv2.imwrite(os.path.join(save_dir, image_name+'_rect.jpg'), rect_img)
                
            # compute fluorescence of every cell in big_image
            data,trap_imgs, cell_pred_mask, predictions = get_cells_info(big_image, img_GFP, X_aligned, Y_aligned, template_shape, time_point, model, parameters, data)
            data[0]['big_image_data'].append(big_images_data)
            exp_data[z] = data
            
            
            
            # save prediction images   (only timepoint 0)
            if save['save_predict_images'] and time_point == 0:      
                predicted_mask = predict_mask(predictions, parameters['new_class'])
                num_test_examples = len(trap_imgs)
                for i in range(num_test_examples):
                    if i < 9:
                        img_name = image_name+"_trap00"+str(i+1)        
                    else: 
                        img_name = image_name+"_trap0"+str(i+1)
                    save_dir = os.path.join(folders['experiment_results_dir'], "predicted labels")
                    if not os.path.exists(save_dir):
                        os.makedirs(save_dir)
                    save_image_labels(trap_imgs[i][:,:,0], predicted_mask[i,:,:], img_name, save_dir, net_name)
                    

    return exp_data, trap_imgs, cell_pred_mask, predictions


def save_exp_data(exp_data, Zs, save_exp_dir, save):
    """
    Saves experiment information for every Z separately as a '.p' file
    """
    for z in Zs:      
        with open(os.path.join(save_exp_dir,"exp_data_z"+str(z)+".p"), 'wb') as fp:
            pickle.dump(exp_data[z], fp, protocol=pickle.HIGHEST_PROTOCOL)
            
        if save['save_fluorescence_curves']:
            plot_fluorescence_data(exp_data[z], save_exp_dir, z)
    return


def complete_time_points(data, trap_id, cell_id):
    """
    This function complete previous timepoints with zeros in case a new cell was detected in a timepoint > 0
    Input:
    data: list of dictionaries with all cells data
    trap_id: desired trap_id 
    cell_id: desired cell_id 
    """
    max_timepoint = data[0]['max_timepoints']     # get number of timepoints
    timepoints = [t for t in range(max_timepoint)]
    trap_data = get_dict_elements('trap_id', trap_id, data)         # get cells with same 'trap_id'
    cell_data = get_dict_elements('cell_id', cell_id, trap_data)    # get cells with same 'cell_id' in trap_data
    cell_timepoints = [cell['timepoint'] for cell in cell_data]     # get all time points where the cell was predicted
    diff_timepoints = np.setdiff1d(timepoints, cell_timepoints)     # get cell_timepoints that are not in timepoints (mising prediction)
    
    # create cells that were not predicted in all time points. Set value to null
    for timepoint in diff_timepoints:
        cells = {}
        cells['timepoint'] = timepoint
        cells['trap_id'] = trap_id
        cells['cell_area'] =  1e-3
        cells['centroid'] = []
        cells['fluorescence'] = 0
        cells['coordinates'] = []
        cells['bounding_box'] = []
        cells['cell_id'] = cell_id
        cell_data.append(cells)
    cell_data = sorted(cell_data, key=lambda k: k['timepoint'])    # sort by timepoint values
    return cell_data


def plot_fluorescence_data(data, save_exp_dir, z):
    """
    This function plots the fluorescence, fluorescence/area and area curves of cells read from data list of dictionaries
    """
    num_traps = data[0]['num_traps'] # number of traps
    plt.figure(1, figsize=(20, 20))
    plt.figure(2, figsize=(18, 18))
    plt.figure(3, figsize=(18, 18))
    
    for trap_id in range(1, num_traps+1):
        num_cells = data[1][trap_id] # number of ids in trap_id
        for cell_id in range(1,num_cells+1):
            # get cell_id in trap_id and complete missing timepoints where cell-id was not predicted
            cell_data = complete_time_points(data, trap_id, cell_id)
            fluorescence = [cell['fluorescence'] for cell in cell_data]
            area = [cell['cell_area'] for cell in cell_data]
            time = [10*cell['timepoint'] for cell in cell_data]  # time in minutes --> every timepoint means 10 minutes
            
            #  division of lists (fluorescence/area)
            fluorescence_area = [fl/a for fl, a in zip(fluorescence, area)] 
            
            plt.figure(1)
            plt.plot(time, fluorescence_area, 'o-', label = "Trap "+str(trap_id) + " - Cell "+str(cell_id), markersize=2)
            for i in range(cell_id+2, len(time),10):
                plt.annotate(str(trap_id)+"-"+str(cell_id), (time[i], fluorescence_area[i]))

            plt.figure(2)
            plt.plot(time, fluorescence, 'o-', label = "Trap "+str(trap_id) + " - Cell "+str(cell_id), markersize=2)
            for i in range(cell_id+2, len(time), 15):
                plt.annotate(str(trap_id)+"-"+str(cell_id), (time[i], fluorescence[i]))
           
            plt.figure(3)
            plt.plot(time, area, 'o-', label = "Trap "+str(trap_id) + " - Cell "+str(cell_id), markersize=2)
            for i in range(cell_id+2,len(time),15):
                plt.annotate(str(trap_id)+"-"+str(cell_id), (time[i], area[i]))
        
    plt.figure(1)
    plt.xlabel('Time (min)')
    plt.ylabel('Fluorescence/Area') 
    plt.legend(loc='center left', prop={'size': 10}, bbox_to_anchor=(1, 0.5))
    plt.grid()
    plt.savefig(os.path.join(save_exp_dir, "Fluorescence_Area_Results_All_z"+str(z)), bbox_inches='tight', dpi=100)
    plt.close()
    
    plt.figure(2)
    plt.xlabel('Time (min)')
    plt.ylabel('Fluorescence')
    plt.legend(loc='center left', prop={'size': 10}, bbox_to_anchor=(1, 0.5))   
    plt.grid()
    plt.savefig(os.path.join(save_exp_dir, "Fluorescence_Results_All_z"+str(z)))
    plt.close()
    
    plt.figure(3)
    plt.xlabel('Time (min)')
    plt.ylabel('Area')
    plt.legend(loc='center left', prop={'size': 10}, bbox_to_anchor=(1, 0.5))
    plt.grid()  
    plt.savefig(os.path.join(save_exp_dir, "Area_Results_All_z"+str(z)))
    plt.close()
            
    return

def plot_cell_data(data, trap_id, cell_id, save_exp_dir):
    """
    This function plots the fluorescence, fluorescence/area and area curves of cell_id of trap_id read from data
    """
    
    # get cell_id in trap_id and complete missing timepoints where cell-id was not predicted
    cell_data = complete_time_points(data, trap_id, cell_id)
    fluorescence = [cell['fluorescence'] for cell in cell_data]
    area = [cell['cell_area'] for cell in cell_data]
    time = [10*cell['timepoint'] for cell in cell_data]  # time in minutes --> every timepoint means 10 minutes
    
    #  division of lists (fluorescence/area)
    fluorescence_area = [fl/a for fl, a in zip(fluorescence, area)] 
    
    plt.figure(figsize=(18, 14))
    plt.subplot(3,1,1)
    plt.plot(time, fluorescence_area, 'o-', label = "Trap "+str(trap_id) + " - Cell "+str(cell_id), markersize=2)
    for i in range(0,len(time),15):
        plt.annotate(cell_id, (time[i], fluorescence_area[i]))
    plt.ylabel('Fluorescence/Area')
    plt.grid()
    
    plt.subplot(3,1,2)
    plt.plot(time, fluorescence, 'o-', label = "Trap "+str(trap_id) + " - Cell "+str(cell_id), markersize=2)
    for i in range(0,len(time),15):
        plt.annotate(cell_id, (time[i], fluorescence[i]))
    plt.ylabel('Fluorescence')
    plt.grid()
    
    plt.subplot(3,1,3)
    plt.plot(time, area, 'o-', label = "Trap "+str(trap_id) + " - Cell "+str(cell_id), markersize=2)
    for i in range(0,len(time),15):
        plt.annotate(cell_id, (time[i], area[i]))
    plt.xlabel('Time (min)')
    plt.ylabel('Area')
    plt.grid()
    
    plt.suptitle('Trap ' + str(trap_id)+ ' - Cell ' + str(cell_id))
    plt.savefig(os.path.join(save_exp_dir,"Fluorescence_Results_Trap"+str(trap_id)+"_Cell"+str(cell_id)), bbox_inches='tight',dpi=100)
    plt.close()
    
    return


def plot_trap_data(data, trap_id, save_exp_dir):
    """
    This function plots the fluorescence, fluorescence/area and area curves of all cells in trap_id read from data
    """
    num_cells = data[1][trap_id] # number of ids in trap_id
    plt.figure(figsize=(18, 18))

    for cell_id in range(1,num_cells+1):
        # get cell_id in trap_id and complete missing timepoints where cell-id was not predicted
        cell_data = complete_time_points(data, trap_id, cell_id)
        fluorescence = [cell['fluorescence'] for cell in cell_data]
        area = [cell['cell_area'] for cell in cell_data]
        time = [10*cell['timepoint'] for cell in cell_data]  # time in minutes --> every timepoint means 10 minutes
        
        #  division of lists (fluorescence/area)
        fluorescence_area = [fl/a for fl, a in zip(fluorescence, area)] 
        
        plt.subplot(3,1,1)
        plt.plot(time, fluorescence_area, 'o-', label = "Trap "+str(trap_id) + " - Cell "+str(cell_id), markersize=2)
        for i in range(cell_id+2, len(time),10):
            plt.annotate(cell_id, (time[i], fluorescence_area[i]))

        plt.subplot(3,1,2)
        plt.plot(time, fluorescence, 'o-', label = "Trap "+str(trap_id) + " - Cell "+str(cell_id), markersize=2)
        for i in range(cell_id+2,len(time),15):
            plt.annotate(cell_id, (time[i], fluorescence[i]))
       
        plt.subplot(3,1,3)
        plt.plot(time, area, 'o-', label = "Trap "+str(trap_id) + " - Cell "+str(cell_id), markersize=2)
        for i in range(cell_id+2,len(time),15):
            plt.annotate(cell_id, (time[i], area[i]))
    
    plt.subplot(3,1,1)
    plt.ylabel('Fluorescence/Area') 
    plt.legend(loc='center left', prop={'size': 10}, bbox_to_anchor=(1, 0.5))
    plt.grid()

    plt.subplot(3,1,2)
    plt.ylabel('Fluorescence')
    plt.legend(loc='center left', prop={'size': 10}, bbox_to_anchor=(1, 0.5))   
    plt.grid()
    
    plt.subplot(3,1,3)
    plt.xlabel('Time (min)')
    plt.ylabel('Area')
    plt.legend(loc='center left', prop={'size': 10}, bbox_to_anchor=(1, 0.5))
    plt.grid()  
    plt.savefig(os.path.join(save_exp_dir,"Results_Trap_"+str(trap_id)))
    plt.close()
    
def make_kymograph(data, save_exp_dir):
    """
    This function makes the Kymograph of fluorescence/area over time (x axis) for all cells ids (y axis)
    """
    kymograph_fluorescence_area = []
    num_traps = data[0]['num_traps'] # number of traps
    for trap_id in range(1, num_traps+1):
        num_cells = data[1][trap_id] # number of ids in trap_id
        for cell_id in range(1,num_cells+1):
            cell_data = complete_time_points(data, trap_id, cell_id)
            fluorescence = [float(cell['fluorescence']) for cell in cell_data]
            area = [cell['cell_area'] for cell in cell_data]
            
            # division of lists (fluorescence/area)
            fluorescence_area = [float(fl/a) for fl, a in zip(fluorescence, area)] 
            kymograph_fluorescence_area.append(fluorescence_area)
    
    kymograph_fluorescence_area = np.array(kymograph_fluorescence_area)
    
    plt.figure(figsize=(20, 10))
    plt.imshow(kymograph_fluorescence_area)
    plt.xlabel('timepoint')
    plt.ylabel('cell ID')
    plt.title('Fluorescence/Area Kymograph')
    plt.colorbar()
    plt.savefig(os.path.join(save_exp_dir, "Kymograph_fluorescence_area"))
    plt.close()
    return
    

def cell_prediction_from_data(data, trap_id, cell_id):
    """
    Reconstruct predictions of cells by coordinates saved in data
    """
    cell_data = complete_time_points(data, trap_id, cell_id)
    trap_shape = data[0]['trap_shape']
    predictions = []
    for cell in cell_data:
        predicted_cell = np.zeros(trap_shape)
        coordinates = cell['coordinates']
        for coord in coordinates:
            predicted_cell[coord[0], coord[1]] = 1
        predictions.append(predicted_cell)
    return predictions


def make_prediction_gif(data, trap_id, cell_id, save_exp_dir):
    """
    GIF visualization of the predictions of cell_id of trap_of
    """
    predictions = cell_prediction_from_data(data, trap_id, cell_id)
    fig, ax = plt.subplots()
    ims = []
    for timepoint in range(len(predictions)):    
        im = plt.imshow(predictions[timepoint])
        title = ax.text(0.5,1.05,"Timepoint: {}".format(timepoint), 
                    size=plt.rcParams["axes.titlesize"],
                    ha="center", transform=ax.transAxes, )
        plt.xticks([])
        plt.yticks([])
        ims.append([im, title])
    
    ani = animation.ArtistAnimation(fig, ims, interval=50, blit=True,
                                repeat_delay=1000)
    writer = PillowWriter(fps=5)
    ani.save(os.path.join(save_exp_dir,"Trap_"+str(trap_id)+"_Cell_"+str(cell_id)+".gif"), writer = writer)
    plt.close()

