# Yeast Cell Segmentation in Microstructured Environments with Deep Learning
[![arXiv](https://img.shields.io/badge/cs.CV-arXiv%3A2106.08285-B31B1B.svg)]()
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](LICENSE)

**[Tim Prangemeier](https://www.bcs.tu-darmstadt.de/bcs_team/prangemeiertim.en.jsp), [Christian Wildner](https://www.bcs.tu-darmstadt.de/bcs_team/wildnerchristian.en.jsp), André O. Françani, [Christoph Reich](https://christophreich1996.github.io/) & [Heinz Koeppl](https://www.bcs.tu-darmstadt.de/bcs_team/koepplheinz.en.jsp)**<br/>


<p align="center">
  <img src="/figures/fig_1_2.png"  alt="1" >
</p>
  
<p align="center">
  This repository includes the <b>official</b> implementation of the paper <a href=""> Yeast Cell Segmentation in Microstructured Environments with Deep Learning</a>
</p>


**If you find this research useful in your work, please cite our paper:**

```bibtex
@article{Prangemeier2021,
    title={{Yeast Cell Segmentation in Microstructured Environments with Deep Learning}},
    author={Prangemeier, Tim and Wildner, Christian and Françani, André O. and Reich, Christoph and Koeppl, Heinz},
    journal = {Biosystems},
    volume = {1},
    pages = {1-2},
    year = {2021},
}
```

## U-Net

For the U-Net implementation, please refer to the [U-Net folder](U-Net) (sperate README).

## Mask R-CNN

For the Mask R-CNN implementation, please refer to the [Mask R-CNN folder](Mask_R-CNN) (sperate README).
