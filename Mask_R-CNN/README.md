# Mask R-CNN

Implementation of the Mask R-CNN for cell segmentation.

## Installation
To install all necessary packages simply run:

```shell script
pip install -r requirements.txt
```

## Usage

To run Mask R-CNN please run the main.py script.

```shell script
python main.py
```

Running the script on your device may require some changes in the main.py file, such as the dataset path or CUDA devices to be used.

## References

```bibtex
@inproceedings{He2017,
        title={{Mask R-CNN}},
        author={He, Kaiming and Gkioxari, Georgia and Doll{\'a}r, Piotr and Girshick, Ross},
        booktitle={{Proceedings of the IEEE international conference on computer vision}},
        pages={2961--2969},
        year={2017}
}
```
