import os

os.environ["CUDA_VISIBLE_DEVICES"] = "0"

import torch
import torchvision
from torchvision.models.detection import backbone_utils

from dataset import CellInstanceSegmentation
from model_wrapper import ModelWrapper

if __name__ == '__main__':
    # Init mask r cnn
    backbone = backbone_utils.resnet_fpn_backbone('resnet18', False)
    mask_r_cnn = torchvision.models.detection.MaskRCNN(backbone=backbone, num_classes=3)
    # Print number of parameters
    print("# Mask R-CNN parameters", sum([p.numel() for p in mask_r_cnn.parameters()]))
    # Init optimizer
    mask_r_cnn_optimizer = torch.optim.AdamW(mask_r_cnn.parameters(), lr=1e-04, weight_decay=1e-03)
    # Init model wrapper
    model_wrapper = ModelWrapper(
        mask_r_cnn=mask_r_cnn,
        mask_r_cnn_optimizer=mask_r_cnn_optimizer,
        training_data=CellInstanceSegmentation(
            path=os.path.join("trapped_yeast_cell_dataset", "train"),
            augmentation_p=0.6, two_classes=True),
        test_data=CellInstanceSegmentation(
            path=os.path.join("trapped_yeast_cell_dataset", "train"),
            augmentation_p=0.0, two_classes=True),
        device="cuda"
    )
    model_wrapper.validate()
    # Perform training
    model_wrapper.train(epochs=50)
