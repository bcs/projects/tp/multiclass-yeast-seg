import torch
import torch.nn as nn
from torch.utils.data import Dataset
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from datetime import datetime
import os
from tqdm import tqdm
import numpy as np

import misc
import validation_metric


class ModelWrapper(object):
    """
    Model wrapper class which hold model, optimizer etc. and includes the validation, test and training loops
    """

    def __init__(self, mask_r_cnn: nn.Module, mask_r_cnn_optimizer: torch.optim.Optimizer,
                 training_data: Dataset, test_data: Dataset, device: str = 'cuda',
                 save_data_path: str = "saved_data") -> None:
        # Save parameter
        self.mask_r_cnn = mask_r_cnn
        self.mask_r_cnn_optimizer = mask_r_cnn_optimizer
        self.training_data = training_data
        self.test_data = test_data
        self.device = device
        # Init logger to log training, validation and test metrics
        self.logger = misc.Logger()
        # Make directories to save logs, plots and models during training
        time_and_date = datetime.now().strftime("%d_%m_%Y__%H_%M_%S")
        save_data_path = os.path.join(save_data_path, time_and_date)
        os.makedirs(save_data_path, exist_ok=True)
        self.path_save_models = os.path.join(save_data_path, "models")
        os.makedirs(self.path_save_models, exist_ok=True)
        self.path_save_plots = os.path.join(save_data_path, "plots")
        os.makedirs(self.path_save_plots, exist_ok=True)
        self.path_save_metrics = os.path.join(save_data_path, "metrics")
        os.makedirs(self.path_save_metrics, exist_ok=True)
        # Init epoch
        self.epoch = 0

    def train(self, epochs: int = 10) -> None:
        # Model into training mode
        self.mask_r_cnn.train()
        # Model to device
        self.mask_r_cnn.to(self.device)
        # Init progress bar
        self.progress_bar = tqdm(total=epochs * len(self.training_data))
        # Main loop
        for self.epoch in range(epochs):
            for input, instances, bounding_boxes, classes in self.training_data:
                # Update progress bar
                self.progress_bar.update(n=1)
                # Needed data to device
                input = input.to(self.device)
                instances = instances.to(self.device)
                bounding_boxes = bounding_boxes.to(self.device)
                classes = classes.to(self.device)
                # Reset gradients
                self.mask_r_cnn.zero_grad()
                # Make prediction
                output = self.mask_r_cnn([input], [{"boxes": bounding_boxes, "labels": classes, "masks": instances}])
                # Get full loss
                loss = output['loss_classifier'] + output['loss_box_reg'] + output['loss_mask'] + output[
                    'loss_objectness'] + output['loss_rpn_box_reg']
                # Calc gradients
                loss.backward()
                # Optimizer model
                self.mask_r_cnn_optimizer.step()
                # Show losses in progress bar
                self.progress_bar.set_description(
                    "L={:.4f}, L. Class={:.4f} L. BB={:.4f} L. Mask={:.4f} L. Obj.={:.4f} L. RPN={:.4f}".format(
                        loss.item(), output['loss_classifier'].item(), output['loss_box_reg'].item(),
                        output['loss_mask'].item(), output['loss_objectness'].item(), output['loss_rpn_box_reg'].item()
                    ))
            # Save model
            torch.save(self.mask_r_cnn.state_dict(),
                       os.path.join(self.path_save_models, 'Mask_R_CNN_{}.pt'.format(self.epoch)))
            # Validate
            self.validate()

    def validate(self, metric: nn.Module = validation_metric.CellIoU()) -> None:
        # Init cell iou
        cell_iou = []
        # Model into eval mode
        self.mask_r_cnn.eval()
        # Model to device
        self.mask_r_cnn.to(self.device)
        # Init counter
        counter = 0
        # Loop over all training data
        for input, instances, bounding_boxes, classes in self.test_data:
            # Needed data to device
            input = input.to(self.device)
            # Make prediction
            output = self.mask_r_cnn([input])
            # Get scores
            scores = output[0]["scores"].detach().cpu().numpy()
            # Get indexes of all objects
            indexes = np.argwhere(scores >= 0.5)[:, 0]
            # Case objects are detected
            if indexes.shape[0] > 0:
                # Get masks [N, 1, H, W]
                masks = output[0]["masks"].detach().cpu()[indexes]
                # Get bounding boxes
                bounding_boxes = output[0]["boxes"].detach().cpu()[indexes]
                # Get classification
                classifications = output[0]["labels"].detach().cpu()[indexes]
                # Calc cell iou
                cell_iou.append(metric(masks, instances, classifications, classes).item())
                # Plot results
                misc.plot_instance_segmentation_overlay_instances_bb_classes(image=input, class_labels=classifications,
                                                                             bounding_boxes=bounding_boxes,
                                                                             instances=(masks > 0.5), show=False,
                                                                             save=True,
                                                                             file_path=os.path.join(
                                                                                 self.path_save_plots,
                                                                                 "test_{}_{}_is_bb.png".format(
                                                                                     self.epoch,
                                                                                     counter)))
                misc.plot_instance_segmentation_overlay_instances(image=input, class_labels=classifications,
                                                                  instances=(masks > 0.5), show=False,
                                                                  save=True,
                                                                  file_path=os.path.join(self.path_save_plots,
                                                                                         "test_{}_{}_is.png".format(
                                                                                             self.epoch,
                                                                                             counter)))
                misc.plot_instance_segmentation_labels(instances=(masks > 0.5)[:, 0], class_labels=classifications,
                                                       bounding_boxes=bounding_boxes, show=False, save=True,
                                                       white_background=True, show_class_name=False,
                                                       file_path=os.path.join(self.path_save_plots,
                                                                              "test_{}_{}_is_bb_no_overlay.png".format(
                                                                                  self.epoch, counter)))
                misc.plot_instance_segmentation_map_label(instances=(masks > 0.5)[:, 0], class_labels=classifications,
                                                          show=False, save=True,
                                                          white_background=True,
                                                          file_path=os.path.join(self.path_save_plots,
                                                                                 "test_{}_{}_is_no_overlay.png".format(
                                                                                     self.epoch, counter)))
            # Increment counter
            counter += 1
        # Print cell iou
        cell_iou = np.array(cell_iou)
        print("\nCellIou =", np.mean(cell_iou[~np.isnan(cell_iou)]))
        # Model back into train mode
        self.mask_r_cnn.train()
